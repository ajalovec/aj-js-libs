
module.exports = (function() {
    /**
     * [DELIMITER description]
     * @type {String}
     */
    var DELIMITER = "%";

    /**
     * [getPath description]
     * @param  {[type]} basePath [description]
     * @param  {[type]} path     [description]
     * @return {[type]}          [description]
     */
    var getPath = function(basePath, path)
    {
        if(!basePath) return path || '';
        if(!path)     return basePath;
        
        return basePath.concat('/', (path == '/' ? '' : path));
    };

    /**
     * [replaceParams description]
     * @param  {[type]} params [description]
     * @param  {[type]} str    [description]
     * @return {[type]}        [description]
     */
    var replaceParams = function(params, basePath, str) {
        if(str instanceof Object) {
            for (var i in str) {
                str[i] = replaceParams(params, basePath, str[i]);
            }

            return str;
        }

        if(str) {
            var searches = str.match(new RegExp('%[^/]+', 'gi'));
            if(searches) {
                for(var i in searches) {
                    str = str.replace(
                        new RegExp(searches[i], 'gi'),
                        params[searches[i]] || ""
                    );

                    if(! params[searches[i]])
                        console.log("Parameter " + searches[i] + " don't exits!");
                }
            }
        }
        
        return getPath(basePath, str);
    };

    var STATIC_DIRS = {};

    function PathBuilder(baseName, basePath, paths)
    {
        var _this = this;
        var DIRS = {};
        
        STATIC_DIRS[DELIMITER+baseName] = basePath;

        function addPath(name, path)
        {
            if(name instanceof Object) {
                for (var _n in name) {
                    addPath(_n, name[_n])
                }
            }
            else if(name) {
                this[name] = getPath(basePath, path);
                DIRS[DELIMITER+name] = path;

                STATIC_DIRS[DELIMITER+baseName+'.'+name] = getPath(basePath, path);
            }
            
            return this;
        }

        this.get = function (paths)
        {
            return replaceParams(DIRS, basePath, paths);
        }

        addPath(paths);
    }
    
    PathBuilder.dirs = STATIC_DIRS;

    PathBuilder.get = function(paths)
    {
        return replaceParams(STATIC_DIRS, null, paths);
    }

    PathBuilder.create = function(baseName, basePath, paths)
    {
        return PathBuilder[baseName] = new PathBuilder(baseName, basePath, paths);
    }

    return PathBuilder;
}());
